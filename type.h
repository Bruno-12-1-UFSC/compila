#ifndef TYPE_H
#define TYPE_H

#include <string>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <llvm/IR/Value.h>

enum var_type {
	UNDEFINED_TYPE, VOID_TYPE, INT_TYPE, DOUBLE_TYPE, STRING_TYPE, CHAR_TYPE, BOOL_TYPE
};

inline std::ostream& operator<<(std::ostream& os, var_type vt) {
    switch (vt) {
    	case UNDEFINED_TYPE: os << "UNDEFINED"; break;
    	case VOID_TYPE: os << "VOID"; break;
    	case INT_TYPE: os << "INT"; break;
    	case DOUBLE_TYPE: os << "DOUBLE"; break;
    	case STRING_TYPE: os << "STRING"; break;
    	case CHAR_TYPE: os << "CHAR"; break;
    	case BOOL_TYPE: os << "BOOL"; break;
    }
    return os;
}

class CodeGenContext;
class NStatement;
class NExpression;
class NVariableDeclaration;
class NDeclaration;

typedef std::vector<NStatement*> StatementList;
typedef std::vector<NExpression*> ExpressionList;
typedef std::vector<NDeclaration*> VariableList;

inline void indent(std::ostream& os, unsigned indentation) {
	unsigned counter = 0;
	while (counter++ < indentation) {
		os << "| ";
	}
}

class Node {
public:
	// virtual ~Node() {}
	// virtual llvm::Value* codeGen(CodeGenContext& context) { return NULL; }
    // virtual void print(std::ostream& os, unsigned indentation) const {os << "Node" << std::endl;};
};

class NExpression : public Node {
public:
	virtual void print(std::ostream& os, unsigned indentation) const = 0 /*{os << "NExpression" << std::endl;}*/;
};

class NStatement : public Node {
public:
	virtual void print(std::ostream& os, unsigned indentation) const = 0 /*{os << "NStatement" << std::endl;}*/;
};

inline std::ostream& operator<<(std::ostream& os, const ExpressionList& dt) {
    os << "[";
    for (std::size_t i = 0; i < dt.size(); i++) {
        dt[i]->print(os, 0);
        os << (i == dt.size() - 1 ? "" : ", ");
    }
    return os << "]";
}

class NBlock : public NExpression {
public:
	StatementList statements;
	NBlock() { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NBlock& dt);
	void print(std::ostream& os, unsigned indentation) const {
		indent(os, indentation);
		os << "NBlock statements:" << std::endl;

		for (auto stmt : statements) {
			if (stmt != nullptr) {
				stmt->print(os, indentation+1);
				os << std::endl;
			} else {
				os << "Null stmt" << std::endl;
			}
		}
	}

	friend std::ostream& operator<<(std::ostream& os, NBlock const& dt) {
        dt.print(os, 0);
        return os;
    }
};

class NInteger : public NExpression {
public:
	long long value;
	NInteger(long long value) : value(value) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NInteger& dt);
	void print(std::ostream& os, unsigned indentation) const {os << "{NInteger: " << value << "}";}
};

class NDouble : public NExpression {
public:
	double value;
	NDouble(double value) : value(value) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NDouble& dt);
	void print(std::ostream& os, unsigned indentation) const {os << "{NDouble: " << value << "}";}
};

class NChar : public NExpression {
public:
	char value;
	NChar(char value) : value(value) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NChar& dt);
	void print(std::ostream& os, unsigned indentation) const {os << "{NChar: " << value << "}";}
};

class NString : public NExpression {
public:
	std::string value;
	NString(std::string value) : value(value) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NString& dt);
	void print(std::ostream& os, unsigned indentation) const {os << "{NString: " << value << "}";}
};

class NVoid : public NExpression {
public:
	// NString(std::string value) : value(value) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NString& dt);
	void print(std::ostream& os, unsigned indentation) const {os << "{NVoid}";}
};

class NIdentifier : public NExpression {
public:
	std::string name;
	NIdentifier(const std::string& name) : name(name) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NIdentifier& dt);
	void print(std::ostream& os, unsigned indentation) const {os << "{NIdentifier: " << name << "}";}
};

class NDeclaration : public NStatement {};

class NVariableDeclaration : public NDeclaration {
public:
	const var_type type;
	NIdentifier& id;
	NExpression *assignmentExpr;
	NVariableDeclaration(const var_type& type, NIdentifier& id) :
		type(type), id(id) { assignmentExpr = nullptr; }
	NVariableDeclaration(const var_type& type, NIdentifier& id, NExpression *assignmentExpr) :
		type(type), id(id), assignmentExpr(assignmentExpr) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NVariableDeclaration& dt);
	void print(std::ostream& os, unsigned indentation) const {
		indent(os, indentation);
		os << "{NVariableDeclaration  type = " << type << "  id = ";
		id.print(os, indentation);
		os << "  assignmentExpr = ";
		if (assignmentExpr == nullptr) {
			os << "UNDEFINED";
		} else {
			assignmentExpr->print(os, indentation);
		}
		os << "}";
	}
};

class NArrayDeclaration : public NDeclaration {
public:
	const var_type type;
	NIdentifier& id;
	unsigned size;
	ExpressionList values;
	NArrayDeclaration(const var_type& type, NIdentifier& id, unsigned size) :
		type(type), id(id), size(size) { }
	NArrayDeclaration(const var_type& type, NIdentifier& id, ExpressionList& values) :
		type(type), id(id), values(values) {
			size = values.size();
		}
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	void print(std::ostream& os, unsigned indentation) const {
		indent(os, indentation);
		os << "{NArrayDeclaration  type = " << type << "  id = ";
		id.print(os, indentation);
		os << "  size = " << size;
		os << "  values = " << values;
		os << "}";
	}
};

inline std::ostream& operator<<(std::ostream& os, const VariableList& dt) {
    os << "[";
    for (std::size_t i = 0; i < dt.size(); i++) {
        dt[i]->print(os, 0);
        os << (i == dt.size() - 1 ? "" : ", ");
    }
    return os << "]";
}

class NBinaryOperator : public NExpression {
public:
	NExpression& lhs;
	int op;
	NExpression& rhs;
	NBinaryOperator(NExpression& lhs, int op, NExpression& rhs) :
		lhs(lhs), op(op), rhs(rhs) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NBinaryOperator& dt);
	void print(std::ostream& os, unsigned indentation) const {
		os << "{NBinaryOperator: {";
		lhs.print(os, indentation);
		os << " " << op << " ";
		rhs.print(os, indentation);
		os << "}}";
	}
};

class NUnaryLOperator : public NExpression {
public:
	int op;
	NExpression& rhs;
	NUnaryLOperator(int op, NExpression& rhs) :
		rhs(rhs), op(op) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NUnaryLOperator& dt);
	void print(std::ostream& os, unsigned indentation) const {os << "NUnaryLOperator";}
};

class NFunctionCall : public NExpression {
public:
	const NIdentifier& id;
	ExpressionList arguments;
	NFunctionCall(const NIdentifier& id, ExpressionList& arguments) :
		id(id), arguments(arguments) { }
	NFunctionCall(const NIdentifier& id) : id(id) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NFunctionCall& dt);
	void print(std::ostream& os, unsigned indentation) const {
		indent(os, indentation);
		os << "NFunctionCall";
		os << "  id = ";
		id.print(os, indentation);
		os << "  arguments = " << arguments;
	}
};

class NAssignment : public NExpression {
public:
	NIdentifier& lhs;
	NExpression& rhs;
	unsigned index = 0;
	NAssignment(NIdentifier& lhs, NExpression& rhs) : lhs(lhs), rhs(rhs) { }
	NAssignment(NIdentifier& lhs, unsigned index, NExpression& rhs) : lhs(lhs), index(index), rhs(rhs) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NAssignment& dt);
	void print(std::ostream& os, unsigned indentation) const {
		os << "{NAssignment: {";
		lhs.print(os, indentation);
		os << " [" << index << "] = ";
		rhs.print(os, indentation);
		os << "}}";
	}
};

class NExpressionStatement : public NStatement {
public:
	NExpression& expression;
	NExpressionStatement(NExpression& expression) :
		expression(expression) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NExpressionStatement& dt);
	void print(std::ostream& os, unsigned indentation) const {
		indent(os, indentation);
		os << "NExpressionStatement  expression = ";
		expression.print(os, indentation);
	}
};

class NFunctionDeclaration : public NStatement {
public:
	const var_type type;
	const NIdentifier& id;
	VariableList arguments;
	const NBlock& block;
	const NExpression& return_expr;
	NFunctionDeclaration(const var_type& type, const NIdentifier& id, const VariableList& arguments, const NBlock& block, const NExpression& return_expr) :
		type(type), id(id), arguments(arguments), block(block), return_expr(return_expr) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NFunctionDeclaration& dt);
	void print(std::ostream& os, unsigned indentation) const {
		indent(os, indentation);
		os << "NFunctionDeclaration";
		os << "  type = " << type;
		os << "  id = ";
		id.print(os, indentation);
		os << "  arguments = " << arguments;
		os << "  block =" << std::endl;
		block.print(os, indentation+1);
		indent(os, indentation+1);
		os << "return = ";
		return_expr.print(os, indentation);
	}
};

class NIfStatement : public NStatement {
public:
	NExpression& condition;
	const NBlock& trueBlock;
	const NBlock& falseBlock;
	NIfStatement(NExpression& condition, const NBlock& trueBlock, const NBlock& falseBlock) :
		condition(condition), trueBlock(trueBlock), falseBlock(falseBlock) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NIfStatement& dt);
	void print(std::ostream& os, unsigned indentation) const {
		indent(os, indentation);
		os << "NIfStatement:  condition = ";
		condition.print(os, indentation);
		os << std::endl;
		indent(os, indentation+1);
		os << "trueBlock = " <<  std::endl;
		trueBlock.print(os, indentation+2);
		indent(os, indentation+1);
		os << "falseBlock = " <<  std::endl;
		falseBlock.print(os, indentation+2);
	}
};

class NWhileStatement : public NStatement {
public:
	NExpression& condition;
	NBlock& block;
	NWhileStatement(NExpression& condition, NBlock& block) :
		condition(condition), block(block) { }
	// virtual llvm::Value* codeGen(CodeGenContext& context);
	// friend std::ostream& operator<<(std::ostream& os, const NWhileStatement& dt);
	void print(std::ostream& os, unsigned indentation) const {
		indent(os, indentation);
		os << "NWhileStatement:  condition = ";
		condition.print(os, indentation);
		os << "  block = " <<  std::endl;
		block.print(os, indentation+1);
	}
};

struct type_value_object {
	std::string name = "";
	var_type type;
	union {
		int int_val;
		double double_val;
		char* string_val;
		char char_val;
		bool bool_val;
	};

	void set_value(const int& v) { int_val = v; }
	void set_value(const double& v) { double_val = v; }
	void set_value(const char& v) { char_val = v; }
    void set_value(char *v) { string_val = v; }
    void set_value(const bool& v) { bool_val = v; }

	void copy_value(const type_value_object& other) {
		if (type != UNDEFINED_TYPE and type != other.type) {
			std::cout << "Atenção: ignorando cópia de variáveis com tipos diferentes." << std::endl;
		} else {
            type = other.type;
            switch (type) {
                case INT_TYPE: int_val = other.int_val;          break;
                case DOUBLE_TYPE: double_val = other.double_val; break;
                case STRING_TYPE: string_val = other.string_val; break;
                case CHAR_TYPE: char_val = other.char_val;       break;
                case BOOL_TYPE: bool_val = other.bool_val;       break;
                default: break;
            }
		}
	}

	type_value_object operator+(const type_value_object& other) {
		type_value_object r;
		r.type = type;
        switch (type) {
            case INT_TYPE: r.int_val = int_val + other.int_val;             break;
            case DOUBLE_TYPE: r.double_val = double_val + other.double_val; break;
            default: break;
        }
        return r;
	}
	type_value_object operator-(const type_value_object& other) {
		type_value_object r;
		r.type = type;
        switch (type) {
            case INT_TYPE: r.int_val = int_val - other.int_val;             break;
            case DOUBLE_TYPE: r.double_val = double_val - other.double_val; break;
            default: break;
        }
		return r;
	}
	type_value_object operator*(const type_value_object& other) {
		type_value_object r;
		r.type = type;
        switch (type) {
            case INT_TYPE: r.int_val = int_val * other.int_val;             break;
            case DOUBLE_TYPE: r.double_val = double_val * other.double_val; break;
            default: break;
        }
		return r;
	}
	type_value_object operator/(const type_value_object& other) {
		type_value_object r;
		r.type = type;
        switch (type) {
            case INT_TYPE: r.int_val = int_val / other.int_val;             break;
            case DOUBLE_TYPE: r.double_val = double_val / other.double_val; break;
            default: break;
        }
		return r;
	}

	friend std::ostream & operator <<(std::ostream & s, const type_value_object & o) {
		s << "{\"" << o.name << "\", " << o.type << ", ";
		switch (o.type) {
			case INT_TYPE:       s << o.int_val;        break;
			case DOUBLE_TYPE:    s << o.double_val;    	break;
			case CHAR_TYPE:      s << o.char_val;       break;
            case STRING_TYPE:    s << o.string_val;    	break;
            case BOOL_TYPE:      s << o.bool_val;       break;
            case UNDEFINED_TYPE: s << "-";          	break;
            case VOID_TYPE:      s << "-";              break;
		}
		return s << "}";
	}
};

struct type_value_array {
	var_type type;
	std::string name = "";

	const type_value_object& operator[](size_t idx) const {
		return values[idx];
	}

	void set_value(size_t idx, type_value_object data) {
		if (idx > _size) {
			std::cout << "Erro: posição " << idx << " inválida para array de tamanho " << _size <<  std::endl;
		} else if (type != UNDEFINED_TYPE and data.type != type) {
			std::cout << "Erro: tipo " << data.type << " incompatível com array do tipo " << type <<  std::endl;
		} else {
			values[idx] = data;
		}
	}

	void set_size(size_t new_size) {
		_size = new_size;
		values.resize(new_size);
	}

	size_t size() {
		return _size;
	}

	friend std::ostream& operator<<(std::ostream& os, type_value_array & vt) {
	    os << "{" << vt.name << ", " << vt._size << ", " << vt.type << ", {";
	    for (auto element : vt.values) {
	    	os << element << ", ";
	    }
	    os << "}}";

	    return os;
	}

protected:
	size_t _size;
	std::vector<type_value_object> values;
};

struct function_object {
	var_type type;
	type_value_array parameter_list;

	friend std::ostream& operator<<(std::ostream& os, function_object & vt) {
	    os << "{" << vt.type;


	    os << "}";

	    return os;
	}
};

typedef struct {
	int                 int_val;
	double              double_val;
	std::string         string_val;
	char                char_val;
	var_type            type;
	type_value_object   type_value;
	type_value_array	type_value_list;

	Node *node;
	NBlock *block;
	NExpression *expr;
	NStatement *stmt;
	NIdentifier *ident;
	NDeclaration *var_decl;
	std::vector<NDeclaration*> *varvec;
	std::vector<NExpression*> *exprvec;
	std::string *string;
	int token;
} YYSTYPE;

static std::unordered_map<std::string, type_value_object> declared_variables;
static std::unordered_map<std::string, type_value_array> declared_arrays;
static std::unordered_map<std::string, function_object> declared_functions;

#endif
